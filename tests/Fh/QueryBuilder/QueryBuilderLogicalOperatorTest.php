<?php

namespace Fh\QueryBuilder;

use Mockery as m;
use Fh\QueryBuilder\QueryBuilder;

class QueryBuilderLogicalOperatorTest extends QueryBuilderTestBase {

    public function test_it_can_set_wheres() {
        $strTestUri = '/api/v1/letters?betweenLetterId[]=4&betweenLetterId[]=8&whereFirstName=Jon&operator=or';

        $qb = $this->createQueryBuilder($strTestUri);
        $qb->initializeWherePrefixes();
        $qb->setWheres();
        $strSql = $qb->toSql();
        $strExpected = 'select * from "Table" where "Table"."LetterId" between ? and ? or "Table"."FirstName" = ?';
        $this->assertEquals($strExpected,$strSql);

        $aBindings = $qb->getBindings();
        $aExpected = [4,8,'Jon'];
        $this->assertEquals($aExpected,$aBindings);
    }

    public function test_it_can_get_a_record_count_for_all_records() {
        $strTestUri = '/api/v1/letters?likeFirstName=Jon&likeLastName=Watson&operator=or';

        $qb = $this->createQueryBuilder($strTestUri);
        $qb->initializeWherePrefixes();
        $qb->setWheres();
        $countBuilder = $qb->getCountBuilder();

        $strSql = $countBuilder->toSql();
        $strExpected = 'select count(DISTINCT Table.TestId) as cnt from "Table" where "Table"."FirstName" LIKE ? or "Table"."LastName" LIKE ?';
        $this->assertEquals($strExpected,$strSql);

        $aBindings = $qb->getBindings();
        $aExpected = ['%Jon%','%Watson%'];
        $this->assertEquals($aExpected,$aBindings);
    }

    public function test_it_can_use_normal_queries_with_and() {
        $strTestUri = '/api/v1/letters?betweenLetterId[]=4&betweenLetterId[]=8&whereFirstName=Jon';

        $qb = $this->createQueryBuilder($strTestUri);
        $qb->initializeWherePrefixes();
        $qb->setWheres();
        $strSql = $qb->toSql();
        $strExpected = 'select * from "Table" where "Table"."LetterId" between ? and ? and "Table"."FirstName" = ? and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);

        $aBindings = $qb->getBindings();
        $aExpected = [4,8,'Jon'];
        $this->assertEquals($aExpected,$aBindings);
    }
}
