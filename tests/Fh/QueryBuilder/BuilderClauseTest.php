<?php

namespace Fh\QueryBuilder;

use Mockery as m;
use Fh\QueryBuilder\QueryBuilder;
use Fh\QueryBuilder\BuilderClause;
use Fh\Data\Mapper\US\LetterMapper;

class BuilderClauseTest extends QueryBuilderTestBase {

    public function test_it_can_provide_a_default_value_modifier() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'where','where','=');

        $fn = $w->getDefaultValueModifier();
        $this->assertIsClosure($fn);
    }

    public function test_it_knows_when_a_field_name_indicates_a_relationship() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'where','where','=');
        $strField = 'wherephotos.Caption';
        $bActual = $w->fieldIndicatesRelation($strField);
        $bExpected = TRUE;
        $this->assertEquals($bExpected,$bActual);

        $strField = 'wherephotosCaption';
        $bActual = $w->fieldIndicatesRelation($strField);
        $bExpected = FALSE;
        $this->assertEquals($bExpected,$bActual);
    }

    public function test_it_can_properly_strip_the_prefix_out_of_a_field_name() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'inarray','whereIn');

        $strParameter = 'inarrayFirstName';
        $strExpected = 'FirstName';
        $strActual   = $w->getFieldNameFromParameter($strParameter);
        $this->assertEquals($strExpected,$strActual);
    }

    public function test_it_can_modify_an_array_of_values() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'like','where','LIKE',function($value) {
            return "%$value%";
        });

        $aExpected = ['%this here%','%is%','%a%','%test%'];
        $aValues = ['this here','is','a','test'];
        $aValues = $w->modifyValues($aValues);
        $this->assertEquals($aExpected,$aValues);
    }

    public function test_it_can_modify_a_single_value() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'like','where','LIKE',function($value) {
            return "%$value%";
        });

        $strValue = 'This is a test';
        $strExpected = '%This is a test%';
        $strValue = $w->modifyValues($strValue);
        $this->assertEquals($strExpected,$strValue);
    }

    public function test_it_can_instruct_the_builder_with_an_isnull() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'isnull','whereNull');

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'isnullFirstName');
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where "Table"."FirstName" is null and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);
    }

    public function test_it_can_instruct_the_builder_with_an_isnull_on_a_relation() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'isnull','whereNull');

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'isnullphotos.Name');
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where exists (select * from "ChildTable" where "Table"."TestId" = "ChildTable"."TestId" and "ChildTable"."Name" is null and "ChildTable"."deleted_at" is null) and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);
    }

    public function test_it_can_instruct_the_builder_with_an_isnotnull() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'isnotnull','whereNotNull');

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'isnotnullFirstName');
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where "Table"."FirstName" is not null and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);
    }

    public function test_it_can_instruct_the_builder_with_an_orwhere() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'orwhere','orWhere','=');

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'orwhereFirstName','Jon');
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where ("Table"."FirstName" = ?) and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);

        $aBindings = $builder->getBindings();
        $aExpected = ['Jon'];
        $this->assertEquals($aExpected,$aBindings);
    }

    public function test_it_can_instruct_the_builder_with_a_where() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'where','where','=');

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'whereFirstName','Jon');
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where "Table"."FirstName" = ? and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);

        $aBindings = $builder->getBindings();
        $aExpected = ['Jon'];
        $this->assertEquals($aExpected,$aBindings);
    }

    public function test_it_can_instruct_the_builder_with_an_orderby() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'orderby','orderBy');

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'orderbyFirstName');
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where "Table"."deleted_at" is null order by "Table"."FirstName" asc';
        $this->assertEquals($strExpected,$strSql);
    }

    public function test_it_can_instruct_the_builder_with_a_sortbychild() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new OrderParentByChildBuilderClause($this->qb,'sortbychild');

        // Ascending
        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'sortbychildstatus.ChildId');
        $strSql = $builder->toSql();
        $strExpected = 'select "Table".* from "Table" inner join "ChildTable" on "ChildTable"."TestId" = "Table"."TestId" where "Table"."deleted_at" is null order by "ChildTable"."ChildId" asc';
        $this->assertEquals($strExpected,$strSql);

        // Descending
        $builder = $letter->newQuery();
        $w->processWhere($builder,'sortbychildstatus.ChildId','desc');
        $strSql = $builder->toSql();
        $strExpected = 'select "Table".* from "Table" inner join "ChildTable" on "ChildTable"."TestId" = "Table"."TestId" where "Table"."deleted_at" is null order by "ChildTable"."ChildId" desc';
        $this->assertEquals($strExpected,$strSql);
    }


    public function test_it_can_instruct_the_builder_with_a_groupby() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'groupby','groupBy');

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'groupbyFirstName');
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where "Table"."deleted_at" is null group by "Table"."FirstName"';
        $this->assertEquals($strExpected,$strSql);
    }

    public function test_it_can_instruct_the_builder_with_a_between() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'between','whereBetween');

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'betweenLetterId',[5,7]);
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where "Table"."LetterId" between ? and ? and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);

        $aBindings = $builder->getBindings();
        $aExpected = [5,7];
        $this->assertEquals($aExpected,$aBindings);
    }

    public function test_it_can_instruct_the_builder_with_a_notinarray() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'notinarray','whereNotIn');

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'notinarrayLetterId',[5,7]);
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where "Table"."LetterId" not in (?, ?) and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);

        $aBindings = $builder->getBindings();
        $aExpected = [5,7];
        $this->assertEquals($aExpected,$aBindings);
    }

    public function test_it_can_instruct_the_builder_with_a_inarray() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'inarray','whereIn');

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'inarrayLetterId',[5,7]);
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where "Table"."LetterId" in (?, ?) and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);

        $aBindings = $builder->getBindings();
        $aExpected = [5,7];
        $this->assertEquals($aExpected,$aBindings);
    }

    public function test_it_can_instruct_the_builder_with_a_like() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'like','where','LIKE',function($value) {
            return "%$value%";
        });

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'likeFirstName','Jon');
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where "Table"."FirstName" LIKE ? and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);

        $aBindings = $builder->getBindings();
        $aExpected = ['%Jon%'];
        $this->assertEquals($aExpected,$aBindings);
    }

    public function test_it_can_instruct_the_builder_with_an_orlike() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'orlike','orWhere','LIKE',function($value) {
            return "%$value%";
        });

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'orlikeFirstName','Jon');
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where ("Table"."FirstName" LIKE ?) and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);

        $aBindings = $builder->getBindings();
        $aExpected = ['%Jon%'];
        $this->assertEquals($aExpected,$aBindings);
    }

    public function test_it_can_instruct_the_builder_with_a_greaterthan() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'greaterthan','where','>');

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'greaterthanLetterId',9);
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where "Table"."LetterId" > ? and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);

        $aBindings = $builder->getBindings();
        $aExpected = [9];
        $this->assertEquals($aExpected,$aBindings);
    }

    public function test_it_can_instruct_the_builder_with_a_greaterthanorequalto() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'greaterthanorequalto','where','>=');

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'greaterthanorequaltoLetterId',9);
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where "Table"."LetterId" >= ? and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);

        $aBindings = $builder->getBindings();
        $aExpected = [9];
        $this->assertEquals($aExpected,$aBindings);
    }

    public function test_it_can_instruct_the_builder_with_a_lessthan() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'lessthan','where','<');

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'lessthanLetterId',9);
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where "Table"."LetterId" < ? and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);

        $aBindings = $builder->getBindings();
        $aExpected = [9];
        $this->assertEquals($aExpected,$aBindings);
    }

    public function test_it_can_instruct_the_builder_with_a_lessthanorequalto() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'lessthanorequalto','where','<=');

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processWhere($builder,'lessthanorequaltoLetterId',9);
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where "Table"."LetterId" <= ? and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);

        $aBindings = $builder->getBindings();
        $aExpected = [9];
        $this->assertEquals($aExpected,$aBindings);
    }

    public function test_it_can_instruct_the_builder_with_a_filter() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'filter','byStatus');

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processFilter($builder,'filterByStatus',1);
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where "StatusId" = ? and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);

        $aBindings = $builder->getBindings();
        $aExpected = [1];
        $this->assertEquals($aExpected,$aBindings);
    }

    public function test_it_can_instruct_the_builder_with_a_scope() {
        $this->qb = $this->createQueryBuilder('/api/v1/letters');
        $w = new BuilderClause($this->qb,'scope','byStatus');

        $letter = new TestModel();
        $builder = $letter->newQuery();
        $w->processFilter($builder,'scopeByStatus',1);
        $strSql = $builder->toSql();
        $strExpected = 'select * from "Table" where "StatusId" = ? and "Table"."deleted_at" is null';
        $this->assertEquals($strExpected,$strSql);

        $aBindings = $builder->getBindings();
        $aExpected = [1];
        $this->assertEquals($aExpected,$aBindings);
    }

}
