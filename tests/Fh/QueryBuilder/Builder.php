<?php

namespace Fh\QueryBuilder;

use Illuminate\Database\Eloquent\Builder AS EloquentBuilder;

class Builder extends EloquentBuilder
{
    /**
     * Returns true if this builder's model uses soft deletes.
     * False otherwise.
     * @return boolean
     */
    public function usesSoftDeletes() {
        $model = $this->getModel();
        $aUses = static::classUsesDeep($model);
        return in_array('Illuminate\Database\Eloquent\SoftDeletes',$aUses);
    }

    /**
     * Returns an array of all traits used in the given class,
     * using recursion to traverse all classes and other traits.
     * @param  mixed  $class     class instance, or class path
     * @param  boolean $autoload true to use the autoloader to find traits and classes
     * @return array of sting trait namespaced paths
     */
    public static function classUsesDeep($class, $autoload = true) {
        $traits = [];

        // Get traits of all parent classes
        do {
            $traits = array_merge(class_uses($class, $autoload), $traits);
        } while ($class = get_parent_class($class));

        // Get traits of all parent traits
        $traitsToSearch = $traits;
        while (!empty($traitsToSearch)) {
            $newTraits = class_uses(array_pop($traitsToSearch), $autoload);
            $traits = array_merge($newTraits, $traits);
            $traitsToSearch = array_merge($newTraits, $traitsToSearch);
        };

        foreach ($traits as $trait => $same) {
            $traits = array_merge(class_uses($trait, $autoload), $traits);
        }

        return array_unique($traits);
    }
}
