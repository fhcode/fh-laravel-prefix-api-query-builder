<?php

namespace Fh\QueryBuilder;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Route;
use Illuminate\Database\Connection;
use Illuminate\Database\Query\Expression;

class QueryBuilder {

    // Eloquent model
    protected $model;

    // The query parser member variable
    protected $parser;

    // Function to use when creating a new model so it can be mocked
    protected $modelCreationCallback;

    // Default namespace for model names found in the routeMap
    public $strModelNamespace;

    // Array of abstracted clause types that the builder supports.
    protected $builderClauses = [];

    // Sets the paging style: either 'page=' or 'limit/offset'
    public $pagingStyle;

    // Public access to builder just in case someone wants to modify it before
    // calling paginate
    public $builder;

    // Locale code for translated attributes.
    protected $locale;

    // Saves the default logical operator. Values are empty string '' (for default AND) and 'or' (for OR).
    public $defaultLogicalOperator = '';

    // Tells how to deal with trashed records by default.
    public $bIncludeTrashed = FALSE;
    public $bOnlyTrashed    = FALSE;

    public $tmpTable = null;

    public $count = 0;

    /**
     * Constructs the query builder
     * @param array   $routeToModelMap Mapping of route segment names
     *                to a Model name so that we can resolve
     *                parent-child relationships
     * @param Model   $model   Illuminate\Database\Eloquent\Model
     * @param Request $request Illuminate\Http\Request
     */
    public function __construct(RestMapperInterface $restMapper, Request $request) {
        $this->restMapper        = $restMapper; // for cloning
        $this->request           = $request; // for cloning
        $routeToModelMap         = $restMapper->getRestMap();
        $this->parser            = new QueryParser($routeToModelMap,$request);
        $this->strModelNamespace = config('fh-laravel-api-query-builder.modelNamespace');
        $this->pagingStyle       = config('fh-laravel-api-query-builder.pagingStyle');
        $this->model             = $this->resolveModel();
        $this->builder           = $this->model->newQuery();
        $this->crossdbquery      = $this->request->get('crossdbquery',FALSE);

        if($request->get('operator') == 'or') {
            $this->setDefaultLogicalOperator($request->get('operator'));
        }
        $this->initializeWherePrefixes();
    }

    /**
     * Sets the default conjunction operator for all query strings.
     * @param string $c conjunction
     *        valid values are 'or' and empty string or and ('') for and
     */
    public function setDefaultLogicalOperator($c = '') {
        // This switch case prevents program injection
        switch($c) {
            case '':
            case 'and':
                $this->defaultLogicalOperator = '';
                // Do nothing. This is a normal query.
                return;
            case 'or':
                $this->defaultLogicalOperator = 'or';
                // Turn on trashed so we don't cause logical conflicts
                if($this->builder->usesSoftDeletes()) {
                    $this->builder->withTrashed();
                }
                return;
        }
        throw new QueryBuilderException("Unrecognized QueryBuilder Logical Operator '$c'.");
    }

    /**
     * Sometimes we might need to completely remake the query builder.
     * @return QueryBuilder
     */
    public function remake() {
        return new static($this->restMapper,$this->request);
    }

    /**
     * Resolves the model from the routeToModelMap provided
     * in the constructor.
     * @return void
     */
    public function resolveModel() {
        $strModelName = $this->parser->getModelName();

        $strClassPath = $this->strModelNamespace . '\\' . $strModelName;
        return $this->createModel($strClassPath);
    }

    /**
     * Set up all of the various filter clauses that our API supports.
     * @return void
     */
    public function initializeWherePrefixes() {
        $qb = $this;
        $c = $this->defaultLogicalOperator;
        $this->builderClauses = [
            'isnull'       => new BuilderClause($this, 'isnull',$c.'whereNull')
            ,'isnotnull'   => new BuilderClause($this, 'isnotnull',$c.'whereNotNull')
            ,'orwhere'     => new BuilderClause($this, 'orwhere',$c.'orWhere','=')
            ,'where'       => new BuilderClause($this, 'where',$c.'where','=')
            ,'sortbychild' => new OrderParentByChildBuilderClause($this, 'sortbychild')
            ,'orderby'     => new BuilderClause($this, 'orderby','orderBy')
            ,'groupby'     => new BuilderClause($this, 'groupby','groupBy')

            ,'between'     => new BuilderClause($this, 'between',$c.'whereBetween',null,$this->getValuesToArrayFunction(),false)
            ,'inarray'     => new BuilderClause($this, 'inarray',$c.'whereIn',null,$this->getValuesToArrayFunction(),false)
            ,'notinarray'  => new BuilderClause($this, 'notinarray',$c.'whereNotIn',null,$this->getValuesToArrayFunction(),false)
            ,'like'        => new BuilderClause($this, 'like',$c.'where','LIKE',function($value) {
                return "%$value%";
            },false)
            ,'orlike'      => new BuilderClause($this, 'orlike','orWhere','LIKE',function($value) {
                return "%$value%";
            },false)
            ,'greaterthan' => new BuilderClause($this, 'greaterthan',$c.'where','>')
            ,'lessthan'    => new BuilderClause($this, 'lessthan',$c.'where','<')
            ,'greaterthanorequalto' => new BuilderClause($this, 'greaterthanorequalto',$c.'where','>=')
            ,'lessthanorequalto'    => new BuilderClause($this, 'lessthanorequalto',$c.'where','<=')
        ];
    }

    /**
     * Returns a simple value modifier function for builder
     * clauses that expect array values.
     * @return array
     */
    public function getValuesToArrayFunction() {
        return function($value) {
            if(is_array($value)) return $value;
            return [$value];
        };
    }

    /**
     * Build the request parameters into a query builder.
     * @return QueryBuilder this
     */
    public function build() {

        $this->filterByParentRelation()
             ->withTrashed()
             ->onlyTrashed()
             ->includeRelations()
             ->getIfSingleRecord()
             ->setWheres()
             ->setFilters()
             ->setScopes();

        return $this;
    }

    /**
     * Build the request parameters into a query builder.
     * @return QueryBuilder this
     */
    public function buildForCount() {

        $this->filterByParentRelation()
             ->withTrashed()
             ->onlyTrashed()
             ->includeRelations()
             ->getIfSingleRecord()
             ->setWheres(true)
             ->setFilters()
             ->setScopes();
        return $this;
    }

    /**
     * Returns a builder clone that has been modified to select count(*)
     * instead of returning actual results.
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function getCountBuilder() {
        $counter = $this->remake();
        $counter->setModel($this->model);
        $pkey = $this->model->getKeyName();
        $table = $this->model->getTable();
        $counter->buildForCount();
        if(@$counter->getBuilder()->getQuery()->groups) {
            $counter->getBuilder()->select(new Expression("count($table.$pkey) as cnt"));
        } else {
            $counter->getBuilder()->select(new Expression("count(DISTINCT $table.$pkey) as cnt"));
        }
        return $counter->getBuilder();
    }

    /**
     * Returns the results of the count query, which is a single
     * record with a count property.
     * @param $counter QueryBuilder
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getCount(\Illuminate\Database\Eloquent\Builder $counter = null) {
        if(!$counter) $counter = $this->getCountBuilder();
        if(@$counter->getQuery()->groups) {
            $sql = $counter->toSql();
            $aBindings = $counter->getBindings();
            $sql = "select count(*) as cnt FROM (".$sql.") as tmpGroupCount";
            $result = \DB::select($sql,$aBindings);
            $result = $result[0]->cnt;
        } else {
            $result = $counter->first();
            $result = $result->cnt;
        }
        return $result;
    }

    /**
     * Analyzes the URI segments to determine a nested relationship
     * and restricts access only to members of the parent.
     * @return QueryBuilder this
     */
    public function filterByParentRelation() {
        if(!$this->parser->hasParent()) return $this;
        $strPrimaryKey = $this->model->getKeyName();
        $strKeyValue   = $this->parser->getParentId();
        $strRelationName = $this->parser->getRelationName();
        $record = $this->model->where($strPrimaryKey,'=',intval($strKeyValue))->first();
        $this->builder = $record->$strRelationName();
        $this->model = $this->builder->getModel();
        return $this;
    }

    public function getIfSingleRecord() {
        $id = $this->parser->getResourceId();
        if(is_null($id)) return $this;
        $strTableName = $this->model->getTable();
        $strPrimaryKey = $this->builder->getModel()->getKeyName() ? $this->builder->getModel()->getKeyName() : $this->builder->getModel()->getUUID();
        $this->builder->where($strTableName . '.' . $strPrimaryKey,'=',$id);
        return $this;
    }

    /**
     * Tells the builder to also load relations on the resulting model.
     * @return QueryBuilder this
     */
    public function includeRelations() {
        if($with = $this->parser->request->get('with')) {
            $this->builder->with($with);
        }
        return $this;
    }

    /**
     * When withTrashed=1 is on the query string,
     * soft-deleted models will be included.
     * @return QueryBuilder this
     */
    public function withTrashed() {
        if('1' == $this->parser->request->get('withTrashed')) {
            $this->bIncludeTrashed = TRUE;
            if($this->builder->usesSoftDeletes()) {
                $this->builder->withTrashed();
            }
        }
        return $this;
    }

    /**
     * When onlyTrashed=1 is on the query string, only
     * soft-deleted models will be returned.
     * @return QueryBuilder this
     */
    public function onlyTrashed() {
        if('1' == $this->parser->request->get('onlyTrashed')) {
            $this->bOnlyTrashed = TRUE;
            $this->builder->onlyTrashed();
        }
        return $this;
    }

    /**
     * Walks the input array and adds all appropriate clauses
     * to the builder using the BuilderClause for each.
     * @return QueryBuilder this
     */
    public function setWheres($bForCount = false) {
        $input = $this->parser->fixedInput;

        $fn = $this->getWhereProcessor($bForCount);
        array_walk($input, $fn);
        return $this;
    }

    /**
     * Returns a function for use in setWheres array_walk
     * @return Closure function to add a where clause.
     */
    public function getWhereProcessor($bForCount = false) {
        return function($value, $parameterName) use ($bForCount) {
            foreach($this->builderClauses AS $prefix => $clause) {
                if($this->getPrefixFromParameter($parameterName) === $prefix) {
                    if($bForCount && in_array($prefix,['orderby','sortbychild'])) continue;
                    $clause->processWhere($this->builder,$parameterName,$value,$this->getLocale());
                }
            }
        };
    }

    /**
     * Returns the prefix part from the query string
     *
     * @param string $strQueryString Query string
     * @return string Prefix from query string
     */
    public function getPrefixFromParameter($strQueryString) {
        $prefixFromQueryString = "";
        foreach($this->builderClauses as $prefix => $clause) {
            if (strpos($strQueryString, $prefix) !== false) {
                $prefixFromQueryString = $prefix;
            }
        }
        return $prefixFromQueryString;
    }

    /**
     * Ueses array_walk to loop through all of the filters set on this
     * request and add filter calls to the builder.
     * @return QueryBuilder this
     */
    public function setFilters() {
        $input = $this->parser->request->all();

        $fn = $this->getFilterProcessor();
        array_walk($input, $fn);
        return $this;
    }

    /**
     * NOTE: Scope is an alias for filter.
     * The API should use the filter prefix, not scope. The word 'scope'
     * is ambiguous with many other meanings, and should not be used.
     * It is included here for backward compatability because the 'scope'
     * keyword was added to the API in error.
     *
     * Returns a function for use in setFilters array_walk
     * @return Closure function to add a filter clause.
     */
    public function getFilterProcessor($prefix = 'filter') {
        return function($value, $parameterName) use ($prefix) {
            if(preg_match("/^$prefix/",$parameterName) > 0) {
                $method = preg_replace("/^$prefix/",'',$parameterName);
                $method = lcfirst($method);
                $clause = new BuilderClause($this,$prefix,$method);
                $clause->processWhere($this->builder,$parameterName,$value,$this->getLocale());
            }
        };
    }

    /**
     * Ueses array_walk to loop through all of the filters set on this
     * request and add filter calls to the builder.
     * @return QueryBuilder this
     */
    public function setScopes() {
        $input = $this->parser->request->all();

        $fn = $this->getFilterProcessor('scope');
        array_walk($input, $fn);
        return $this;
    }

    /**
     * Creates a new Model through a callback function so it can be mocked.
     * @param  string $strClassPath class path to Model
     * @return Illuminate\Database\Eloquent\Model
     */
    public function createModel($strClassPath) {
        return new $strClassPath();
    }

    /**
     * Returns the Eloquent model object instance that was
     * used to create a new query.
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getModel() {
        return $this->model;
    }

    /**
     * Returns the target model of the collection that will actually
     * be returned. If the current route is a nested parent/child
     * then the child model will be returned, not the working model
     * that the builder uses for every day use.
     * @return Illuminate\Database\Eloquent\Model
     */
    public function getTargetModel() {
        if($this->parser->hasParent()) {
            $strRelationName = $this->parser->getRelationName();
            if(method_exists($this->getModel(),strtolower($strRelationName))) {
                return $this->getModel()->$strRelationName()->getModel();
            } else return $this->getModel();
        } else {
            return $this->getModel();
        }
    }

    /**
     * Allows you to set the model so it can be mocked.
     * @param Illuminate\Database\Eloquent\Model $model
     */
    public function setModel($model) {
        $this->model = $model;
    }

    /**
     * Return the SQL statement that has been
     * built so far.
     * @return string SQL statement
     */
    public function toSql() {
        return $this->builder->toSql();
    }

    /**
     * Returns the builder's bindings so we can inspect them for testing.
     * @return array bindings
     */
    public function getBindings() {
        return $this->builder->getBindings();
    }

    /**
     * Return the Eloquent Builder object so we can inspect it for testing.
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function getBuilder() {
        return $this->builder;
    }

    /**
     * Sets the builder object so it can be mocked
     * @param Illuminate\Database\Eloquent\Builder $builder
     */
    public function setBuilder($builder) {
        $this->builder = $builder;
    }

    /**
     * Returns the URI query parser
     * @return Fh\QueryBuilder\QueryParserInterface
     */
    public function getParser() {
        return $this->parser;
    }

    /**
     * If you want to set a new parser of your own, do that here.
     * @param Fh\QueryBuilder\QueryParserInterface $parser
     */
    public function setParser($parser) {
        $this->parser = $parser;
    }

    /**
     * Sets the paging style so it can be mocked
     * @param string $style either 'page=' or 'limit/offset'
     */
    public function setPagingStyle($style) {
        $this->pagingStyle = $style;
    }

    /**
     * Gets the paging style
     * @return string $style either 'page=' or 'limit/offset'
     */
    public function getPagingStyle() {
        return $this->pagingStyle;
    }

    /**
     * Setter for the total record count property
     * @param int $count
     */
    public function setCount($count) {
        $this->count = $count;
    }

    /**
     * Helper to make a random string suffix for tmp tables
     * @return string with random characters
     */
    public function makeRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Return results via pagination.
     * Supports any style of pagination you want, including:
     * - laravel default ?page=1 from query string
     * - limit / offset pagination using ?limit=10&offset=30
     * - custom query string names for any of the above parameters
     * @param  int $limit
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function paginateMaria($limit = null) {
        $collection = null;
        $strSelectAll = 'SQL_CALC_FOUND_ROWS `' . $this->builder->getModel()->getTable() . '`.*';
        $conn = $this->builder->getConnection();
        if(!$limit)
            $limit = $this->parser->getLimit();
        $page = $this->parser->getPage();
        $start = $this->parser->getOffset();
        $builder = $this->builder
                           ->select(\DB::raw($strSelectAll))
                           ->skip($start)
                           ->limit($limit);
        $strSql = $builder->toSql();
        $aBindings = $builder->getBindings();
        // Do main query
        $collection = $conn->select($strSql,$aBindings);
        // Must get count immediately after main query.
        $sql = 'SELECT FOUND_ROWS() cnt';
        $cntResults = $conn->select($sql);
        if($cntResults) {
          $this->count = $cntResults[0]->cnt;
        }
        // Now hydrate with relations
        $ret = $builder->hydrate($collection);
        if($with = $this->parser->request->get('with')) {
            $ret->load($with);
        }
        // Get count
        return $ret;
    }

    /**
     * Return results via pagination.
     * Supports any style of pagination you want, including:
     * - laravel default ?page=1 from query string
     * - limit / offset pagination using ?limit=10&offset=30
     * - custom query string names for any of the above parameters
     * @param  int $limit
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function paginate($limit = null) {
        $conName = $this->model->getConnection()->getName();
        if($conName == 'enterprisedb' && !$this->crossdbquery) {
          return $this->paginateMaria($limit);
        }
        if(!$limit)
            $limit = $this->parser->getLimit();
        $page = $this->parser->getPage();
        // Page by paging style
        if($this->pagingStyle == 'page=') {
            return $this->builder->paginate($limit,null,null,$page);
        } else {
            // using limit / offset for paging
            $start = $this->parser->getOffset();
            return $this->builder->skip($start)->limit($limit)->get();
        }
    }

    /**
     * Setter for locale
     * @param string $locale code
     */
    public function setLocale($locale) {
        $this->locale = $locale;
    }

    /**
     * Getter for locale
     * @return string locale code
     */
    public function getLocale() {
        return $this->locale;
    }

}
